import React, { Component } from 'react';
import ReactTransitionGroup from 'react-addons-transition-group'
import './App.css';
import Helmet from 'react-helmet';
import MediaQuery from 'react-responsive';

import styled from 'react-emotion';
import { Box, Flex } from './components/UI/Base';
import Header from './components/Header';
import Footer from './components/Footer';
import Loader from './components/Loader';
import Artist from './components/Artist';

import Start from './components/Start/';
import Artists from './components/Artists/Artists';
import About from './components/About/'
import BuyLink from './components/BuyLink';
import Prefetch from './components/Prefetch';

import { Route } from "react-router-dom";

const MainContainer = styled(Box)`
  padding-top:61px;
  padding-bottom:61px;
  min-height: calc(100vh);
  position: relative;
`

function FirstChild(props) {
  const childrenArray = React.Children.toArray(props.children);
  return childrenArray[0] || null;
}

class App extends Component {

  render() {
    return (
        <div className="App">
          <Helmet>
            <meta charSet="utf-8" />
            <title>Algotfestivalen 20 – 22 Juli 2018</title>
            <meta property="og:url" content="http://algotfestivalen.se/" />
            <meta property="og:type" content="website" />
            <meta property="og:site_name" content="Algotfestivalen" />
            <meta property="og:image" content="http://algotfestivalen.se/algotfestivalen.jpg" />
            <meta name="twitter:image" content="http://algotfestivalen.se/algotfestivalen.jpg" />
            <meta name="twitter:description" content="För åttonde året i rad hälsar vi er välkomna till Algotfestivalen! Temat för 2018 är inget mindre än Korall i alla dess fantastiska färger och former!" />
            <meta property="og:title" content="Algotfestivalen 20 – 22 Juli 2018" />
            <meta property="og:description" content="För åttonde året i rad hälsar vi er välkomna till Algotfestivalen! Temat för 2018 är inget mindre än Korall i alla dess fantastiska färger och former!" />
            <meta name="description" content="För åttonde året i rad hälsar vi er välkomna till Algotfestivalen! Temat för 2018 är inget mindre än Korall i alla dess fantastiska färger och former!" />
          </Helmet>
          <Header></Header>
          <MainContainer>

            <Flex w={1} direction="column">
              <Route
                exact
                path="/"
                children={({ match, ...rest }) => (
                  <ReactTransitionGroup component={FirstChild}>
                    {match && <Start {...rest} />}
                  </ReactTransitionGroup>
              )}/>
              <Route
                 path="/om-festivalen"
                 children={({ match, ...rest }) => (
                   <ReactTransitionGroup component={FirstChild}>
                     {match && <About {...rest} />}
                   </ReactTransitionGroup>
              )}/>
              <Route
                exact={true}
                 path="/artister/"
                 children={({ match, ...rest }) => (
                   <ReactTransitionGroup component={FirstChild}>
                     {match && <Artists artist={match.params.artist} {...rest} />}
                   </ReactTransitionGroup>
              )}/>
              <Route
                 path="/artister/:artist"
                 children={({ match, ...rest }) => (
                   <ReactTransitionGroup component={FirstChild}>
                     {match && <Artist artist={match.params.artist} {...rest} />}
                   </ReactTransitionGroup>
              )}/>
            </Flex>
            <MediaQuery query="(min-width: 1224px)">
              <BuyLink />
            </MediaQuery>
            <Footer></Footer>
          </MainContainer>
          <Prefetch />
          <Loader />
        </div>
    );
  }
}
export default App;
