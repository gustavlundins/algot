import firebase from '../firebase.js';

const ABOUT_LOADING = 'ABOUT_LOADING';
const ABOUT_LOADED = 'ABOUT_LOADED';

const getAbout = () => {
  return firebase.database().ref('about');
}

export const fetchAboutData = () => async dispatch => {
  dispatch({ type: ABOUT_LOADING});

  try {
    const aboutRef = await getAbout();
    const aboutSnapshot =  await aboutRef.once('value');
    const about = aboutSnapshot.val();
    dispatch({ type: ABOUT_LOADED, data: about});
  } catch (e) {

    throw e;
  }
}
