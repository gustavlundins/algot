import firebase from '../firebase.js';

const ARTISTS_LOADING = 'ARTISTS_LOADING';
const ARTISTS_LOADED = 'ARTISTS_LOADED';

const getArtists = () => {
  return firebase.database().ref('artists');
}

const formatArtists = items => {
    let newItems = [];
    for (let item in items) {
      newItems.push({
        id: item,
        slug: items[item].slug,
        title: items[item].title,
        from: items[item].from,
        filter: items[item].filter,
        links: items[item].links,
        day: items[item].day,
        about: items[item].about,
        stage: items[item].stage,
        time: items[item].time,
        facebook: items[item].facebook,
        instagram: items[item].instagram,
        image:items[item].image,
        thumbnail:items[item].thumbnail,
        urlTitle:items[item].urlTitle
      });
    }
    return newItems;
}

export const fetchArtists = () => async dispatch => {
  dispatch({ type: ARTISTS_LOADING});

  try {
    const artistRef = await getArtists();
    const artistSnapshot =  await artistRef.once('value');
    const artists = artistSnapshot.val();

    dispatch({ type: ARTISTS_LOADED, artists: formatArtists(artists)});
  } catch (e) {

    throw e;
  }
}
