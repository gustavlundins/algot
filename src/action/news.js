import firebase from '../firebase.js';

const NEWS_LOADING = 'NEWS_LOADING';
const NEWS_LOADED = 'NEWS_LOADED';

const getNews = () => {
  return firebase.database().ref('news');
}

export const fetchNewsData = () => async dispatch => {
  dispatch({ type: NEWS_LOADING});

  try {
    const newsRef = await getNews();
    const newsSnapshot =  await newsRef.once('value');
    const news = newsSnapshot.val();

    dispatch({ type: NEWS_LOADED, news: news});
  } catch (e) {

    throw e;
  }
}
