import firebase from '../firebase.js';

const START_LOADING = 'START_LOADING';
const START_LOADED = 'START_LOADED';

const getStart = () => {
  return firebase.database().ref('start');
}

export const fetchStartData = () => async dispatch => {
  dispatch({ type: START_LOADING});

  try {
    const startRef = await getStart();
    const startSnapshot =  await startRef.once('value');
    const start = startSnapshot.val();

    dispatch({ type: START_LOADED, data: start});
  } catch (e) {

    throw e;
  }
}
