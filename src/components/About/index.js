import React, { Component, Fragment } from 'react';
import { Flex, Text, Box, InlineBox } from '../UI/Base';
import Heading from '../UI/Heading/Heading';
import BorderBottom from '../Helpers/';
import styled from 'react-emotion';
import Icon from '../UI/Icons/Icon';
import AnimatedWrapper from "../AnimatedWrapper";
import { selectAboutData } from '../../store/about';
import { connect } from 'react-redux';

const Link = styled('a')`

`

const Column = props => (
  <Fragment>
    <Heading align={'left'} fontSize={24} mb={2} is="h2">{props.column.title}</Heading>
    {props.column.text && (
      props.column.text.map((text, index) => (
        <Text key={index} fontSize={16} lineHeight={'24px'} mb={2} align="left">
          {text.text}
        </Text>
      ))
    )}
    {props.column.links && (
      <Text fontSize={16} lineHeight={'24px'} align="left">
        {props.column.links.map((link, index) => (
          <Box key={index}>
            <Link className="agipo standard" href={link.path} target="_blank">{link.text} <InlineBox ml={2}><Icon icon="pil-light-up-right" height={'12'} width={'12'} title={'Pil'} /></InlineBox></Link>
          </Box>
        ))}
      </Text>
    )}
  </Fragment>
)

class AboutComponent extends Component {
  render() {
    return (
      <Flex w={1} className="page" direction="column">
        <Heading className={BorderBottom} py={2} px={[2,2,3]} align="left" fontSize={16} is="h1">Info</Heading>
        <Flex direction={['column','column','row']} className={BorderBottom} w={1} py={[3,3,4]} px={[1,1,0]}>
          {this.props.data && (
            <Fragment>
              <Flex mb={[3,3,0]} w={[1,1,1/3]} px={[1,1,3]} direction="column" align="left" justify="left">
                <Column column={this.props.data.column_1} />
              </Flex>
              <Flex mb={[3,3,0]} w={[1,1,1/3]} px={[1,1,3]} direction="column" align="left" justify="left">
                <Column column={this.props.data.column_2} />
                <Heading mt={3} align={'left'} fontSize={24} mb={2} is="h2">{this.props.data.column_2.title2}</Heading>
                {this.props.data.column_2.text2 && (
                  this.props.data.column_2.text2.map((text, index) => (
                    <Text key={index} fontSize={16} lineHeight={'24px'} mb={2} align="left">
                      {text.text}
                    </Text>
                  ))
                )}
                {this.props.data.column_2.links2 && (
                  <Text fontSize={16} mb={2} lineHeight={'24px'} align="left">
                    {this.props.data.column_2.links2.map((link, index) => (
                      <Box key={index}>
                        <Link className="agipo standard" href={link.path} target="_blank">{link.text} <InlineBox ml={2}><Icon icon="pil-light-up-right" height={'12'} width={'12'} title={'Pil'} /></InlineBox></Link>
                      </Box>
                    ))}
                  </Text>
                )}
              </Flex>
              <Flex w={[1,1,1/3]} px={[1,1,3]} direction="column" align="left" justify="left">
                <Column column={this.props.data.column_3} />
              </Flex>
            </Fragment>
          )}
        </Flex>
        <Flex direction={['column','column','row']} w={1} py={[3,3,4]} px={[1,1,0]}>
          {this.props.data && (
            <Fragment>
              <Flex mb={[3,3,0]} w={[1,1,1/3]} px={[1,1,3]} direction="column" align="left" justify="left">
                <Column column={this.props.data.column_4} />
              </Flex>
              <Flex mb={[3,3,0]} w={[1,1,1/3]} px={[1,1,3]} direction="column" align="left" justify="left">
                <Column column={this.props.data.column_5} />
              </Flex>
              <Flex w={[1,1,1/3]} px={[1,1,3]} direction="column" align="left" justify="left">
                <Column column={this.props.data.column_6} />
              </Flex>
            </Fragment>
          )}
        </Flex>
      </Flex>
    )
  }
}

const mapStateToProps = state => ({
  data: selectAboutData(state)
});

const About = AnimatedWrapper(connect(mapStateToProps)(AboutComponent));
export default About;
