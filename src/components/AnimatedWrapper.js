import React, { Component } from "react";
import * as Animated from "animated/lib/targets/react-dom";

const AnimatedWrapper = WrappedComponent => class AnimatedWrapper
 extends Component {
 constructor(props) {
  super(props);
  this.state = {
   animate: new Animated.Value(0)
  };
 }

 componentWillAppear(cb) {
  Animated.spring(this.state.animate, { toValue: 1 }).start();
  // scrollToComponent(this.AnimatedWrapper, { offset: -61, align: 'top', duration: 750, ease:'linear'});
  cb();
 }
 componentWillEnter(cb) {
  setTimeout(
   () => Animated.spring(this.state.animate, { toValue: 1 }).start(),
   150
  );
  cb();
 }
 componentWillLeave(cb) {
  Animated.spring(this.state.animate, { toValue: 0 }).start();
  setTimeout(() => cb(), 75);
 }
 render() {
  const style = {
   opacity: Animated.template`${this.state.animate}`
  };
  return (
   <Animated.div ref={(Animated) => { this.AnimatedWrapper = Animated; }} style={style} className="animated-page-wrapper">
    <WrappedComponent {...this.props} />
   </Animated.div>
  );
 }
};
export default AnimatedWrapper;
