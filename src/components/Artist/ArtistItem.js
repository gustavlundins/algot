import React, { Component } from 'react';
import { Flex, Text, Box } from '../UI/Base';
import Marquee from '../Marquee';
import ArtistsHeader from '../Artists/ArtistsHeader'
import MediaQuery from 'react-responsive';

import {
  Iframe,
  Video,
  Image,
  Overlay,
  Char,
  Left,
  Right,
  MarqueeWrapper
} from './';

class ArtistItem extends Component {
  state = {
    active: false
  }
  componentWillMount() {
    this.timer = null;
  }
  triggerAnimate = () => {
    this.setState({
      active: true
    })
  }
  render() {

    if(this.timer === null){
      this.timer = setTimeout(this.triggerAnimate, 250);
    }
    const { title, image, from, about, links, slug } = this.props.artist;
    const { active } = this.state;
    const marqueItems = [
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug},
      {title:title.toUpperCase(), slug:slug}
    ];
    return (
      <Overlay direction="column">
        <ArtistsHeader back />
        <Flex direction={['column',null,'row']}>
          <Left backgroundImage={image} position="relative" direction="column" align="center" w={[1,null,1/2]}>
            <MediaQuery query="(max-width: 831px)">
              <Image src={image} alt={title} />
            </MediaQuery>
            <MarqueeWrapper bg={'#ffffff'} w={1}>
              <Marquee bb={false} items={marqueItems} />
            </MarqueeWrapper>
          </Left>
          <Right direction="column" px={[2,null,4]} w={[1,null,1/2]}>
            <Box my={[34,null,92]}>
              <Flex direction={"column"} mb={[2,null,3]}>
                <Text fontSize={[30,30,84]} mb={2}>
                  {title.split("((?<=\\s+)|(?=\\s+))").map((char,index) => (
                    <Char className={active ? 'active' : ''} delay={index < 10 ? '0' + index : index} key={index}>{char}</Char>
                  ))}
                </Text>
                <Text className="Opposit" fontSize={[16,null,24]}>({from})</Text>
              </Flex>
              <Box style={{'text-align':'left'}} w={1} my={[3,null,4]}>
                <Text lineHeight={'1.5em'} textAlign={'left'}>{about}</Text>
              </Box>
              {(links) && (
                <Flex direction="column">
                  {links.map((link,index) => (
                    link.name === 'Spotify' && (
                      <Box mb={[3,null,4]}>
                        <iframe title={link.name + index} src={'https://open.spotify.com/embed?uri=spotify:' + link.link} width="100%" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                      </Box>
                    )
                  ))}
                </Flex>
              )}
              {(links) && (
                links.map((link,index) => (
                  link.name === 'Youtube' && (
                    <Box mb={[3,null,4]}>
                      <Video key={index}>
                        <Iframe width="100%" height="auto"
                          src={'https://www.youtube.com/embed/' + link.link}>
                        </Iframe>
                      </Video>
                    </Box>
                  )
                ))
              )}
              {(links) && (
                links.map((link,index) => (
                  link.name === 'Soundcloud' && (
                    <Box mb={[3,null,4]}>
                      <iframe
                        title={link.name + index}
                        width="100%"
                        height="300"
                        scrolling="no"
                        frameborder="no"
                        allow="autoplay"
                        src={`https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/${link.link}&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true`}>
                      </iframe>
                    </Box>
                  )
                ))
              )}
            </Box>
          </Right>
        </Flex>
      </Overlay>
    )
  }
}

export default ArtistItem;
