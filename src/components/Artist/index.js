import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectArtist } from '../../store/artists';
import ArtistItem from './ArtistItem';
import { Flex, Box } from '../UI/Base';
import styled from 'react-emotion';

class Artist extends Component {
  render() {
    const { currentArtist } = this.props;
    return (
      (currentArtist) && (
        <ArtistItem artist={currentArtist} />
      ) : (
        null
      )
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  currentArtist: selectArtist(state, ownProps.artist)
});

export default connect(mapStateToProps)(Artist);

const Overlay = styled(Flex)`
  @media (min-width: 832px) {
    position: fixed;
    top:60px;
    left:0;
    right:0;
    height: calc(100% - 60px);
    z-index: 5;
  }
`
const Left = styled(Flex)`
  position: relative;
  @media (min-width: 832px) {
    background: url(${props => props.backgroundImage});
    background-size: cover;
    background-position: center center;
    height: calc(100vh - 116px);
    overflow: hidden;
  }
`
const Right = styled(Flex)`
  background: #FFDFDF;
  @media (min-width: 832px) {
    overflow: scroll;
  }
  .Opposit {
    font-family: 'Opposit';
  }
`
const Char = styled('span')`
  display: inline-block;
  transform:skewX(-20deg);
  transition: transform 0.15s ease-out;
  transition-delay: .${props => props.delay}s;
  transform-origin: center bottom 0px;
  &.active {
    transform:skewX(0);
  }
`
const Image = styled('img')`
  width: 100%;
  max-width: 100%;
  max-height: 100%;
  height: auto;
  margin: auto;
`
const Video = styled(Box)`
	position: relative;
  padding-bottom: 56.25%; /* 16:9 */
`
const Iframe = styled('iframe')`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: 0;
`

const MarqueeWrapper = styled(Flex)`
  position: absolute;
  bottom:0;
  left:0;
  right:0;
  border-bottom:1px solid black;
  @media (min-width: 832px) {
    border-bottom:none;
  }
`

export {
  Iframe,
  Video,
  Image,
  Overlay,
  Char,
  Left,
  Right,
  MarqueeWrapper
}
