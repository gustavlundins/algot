import React, { Component, Fragment } from 'react';
import { Flex, Box, Text } from '../UI/Base';
import styled from 'react-emotion';
import {Collapse} from 'react-collapse';
import Heading from '../UI/Heading/Heading';
import Icon from '../UI/Icons/Icon';

const Wrapper = styled(Flex)`
  background-color: #FFDFDF;
`;
const Content = styled(Flex)`
  ${'' /* background: red; */}
  width: 100%;
  text-align: left;
  max-width: 1500px;
  position: relative;
`;
const Image = styled('img')`
  width: 100%;
  height: 100%;
  position: relative;
  top:0;
	box-shadow: 0 2px 4px 0 #D6B4E2, 0 18px 50px 0 rgba(0,0,0,0.1);
`;
const ArtistText = styled(Box)`
  max-width: 426px;
`

class ArtistContent extends Component {

  static defaultProps = {
    isActive: false
  }

  render() {
    const img = this.props.item.image;
    return (
      <Wrapper w={1} align="center" justify="center" className={this.props.active ? "active" : ""}>
        <Collapse isOpened={this.props.active} springConfig={{stiffness: 100, damping: 20}}>
          <Content direction={['column','column','row']} w={1} py={[2,2,3]} px={[2,2,3]}>
            <Flex w={[1,1,1/2]} pr={[0,0,3]}>
              <Image className={this.props.active ? "active" : ""} src={img}></Image>
            </Flex>
            <Flex w={[1,1,1/2]} pt={3} pl={[0,0,4]} justify="center">
              <ArtistText>
                <Heading fontSize={36} mb={3} is="h3">About</Heading>
                <Text lineHeight="24px" fontSize={16}>{this.props.item.about}</Text>
                <Flex mt={3} w={1}>
                  <Flex lineHeight="24px" fontSize={16} w={1/2}>
                    <Text lineHeight="24px" fontSize={16}>
                      {this.props.item.day} <br/>
                      {this.props.item.stage} <br/>
                      00:00
                    </Text>
                  </Flex>
                  <Flex lineHeight="24px" fontSize={16} w={1/2}>
                    <Text lineHeight="24px" fontSize={16}>
                      {this.props.item.website && (
                        <Fragment>
                          <a href={this.props.item.website}>Hemsida <Icon icon="pil-light" height={16} width={16} title={'Pil'} /></a><br/>
                        </Fragment>
                      )}
                      {this.props.item.instagram && (
                        <Fragment>
                          <a href={this.props.item.instagram}>Instagram <Icon icon="pil-light" height={16} width={16} title={'Pil'} /></a><br/>
                        </Fragment>
                      )}
                      {this.props.item.facebook && (
                        <Fragment>
                          <a href={this.props.item.facebook}>Facebook <Icon icon="pil-light" height={16} width={16} title={'Pil'} /></a><br/>
                        </Fragment>
                      )}
                  </Text>
                  </Flex>
                </Flex>
              </ArtistText>
            </Flex>
          </Content>
        </Collapse>
      </Wrapper>
    )
  }
}

export default ArtistContent
