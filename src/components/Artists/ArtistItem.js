import React, { Component } from 'react';
import { Box } from '../UI/Base';
import styled from 'react-emotion';
import BorderBottom from '../Helpers'
import scrollToComponent from 'react-scroll-to-component';
import { Link } from "react-router-dom";

const ItemHeader = styled(Box)`
  text-align: left;
  line-height: 104px;
  position: relative;
  cursor: pointer;
  transition:padding-bottom .3s ease;
  &.active {
    padding-bottom:40px;
    @media (min-width: 1224px) {
      padding-bottom:140px;
    }
  }
  .title {
    transition: transform .05s ease-in;
  }
  &:hover {
    .arrow {
      display: block;
    }
    .title {
      transform: skew(-12deg);
    }
  }
`;

const From = styled('span')`
  font-family: 'Opposit';
  font-size: 25px;
  position: absolute;
  top:25px;
`;

const Title = styled('span')`
  position: relative;
  line-height: 1;
  display: inline-block;
  margin-right: 5px;
  span:nth-child(1),span:nth-child(2){
    z-index: 1;
    position: absolute;
    top:0;
    left:0;
    width: 100%;
    transform:translateY(0%);
    -webkit-transform:translateY(0%);
    transition: transform 350ms linear;
    transition-delay: 150ms;
    will-change: transform;
  }
  ${'' /* &.active {
    span:nth-child(1){
      transform:translateY(100%);
      -webkit-transform:translateY(100%);
    }
    span:nth-child(2){
      transform:translateY(200%);
      -webkit-transform:translateY(200%);
    }
  } */}
`;

class ArtistItem extends Component {

  static defaultProps = {
    from: '',
    title: ''
  }

  constructor(props){
    super(props);
    this.state = {
      isActive: this.props.active,
    }
  }

  componentDidMount() {
    if(this.props.active){
      scrollToComponent(this.Header, { offset: -61, align: 'top', duration: 1250, ease:'inOutQuint'});

    }
  }

   render() {
     const { slug, title, from } = this.props.item;
     const { borderBottom } = this.props
     return (
       <div className={borderBottom ? BorderBottom : null}>
        <Link to={slug}>
          <ItemHeader fontSize={[30,30,104]} ref={(ItemHeader) => { this.Header = ItemHeader; }} py={[3,3,4]} pl={[2,2,3]} pr={[2,2,2]} w={1} align="left">
            <Title className={this.state.isActive ? 'active title' : 'title'}>
              <span>{title}</span>
              <span>{title}</span>
              <span>{title}</span>
            </Title>{ }
            <From>
              <span>({from})</span>
            </From>
          </ItemHeader>
        </Link>
      </div>
     )
   }
}

export default ArtistItem;
