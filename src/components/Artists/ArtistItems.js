import React, { Component } from "react";
import { Box } from "../UI/Base";
import ArtistItem from "./ArtistItem";
import { selectArtists } from '../../store/artists';
import { connect } from 'react-redux';

class ArtistItems extends Component {
  static defaultProps = { sort: "friday", artists: [] };


  render() {
    const { artists, artist, sort } = this.props;
    return (
      <Box>
        {artists.map((item, index) => {
          let active = false;
          if (item.filter === sort && sort !== "all") {
            return false;
          }
          if(item.slug.replace('/artister/', '') === artist){
            active = true;
          }
          return (
            <ArtistItem
              pt={4}
              pb={3}
              pl={2}
              pr={2}
              w={1}
              borderBottom={artists.length !== (index + 1)}
              align="left"
              key={item.id}
              item={item}
              active={active}
            />
          );
        })}
      </Box>
    );
  }
}

const mapStateToProps = state => ({
  artists: selectArtists(state)
});

export default connect(mapStateToProps)(ArtistItems);
