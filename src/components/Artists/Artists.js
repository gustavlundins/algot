import React, { Component, Fragment } from 'react';
import { Flex } from '../UI/Base';
import ArtistsHeader from './ArtistsHeader'
import ArtistItems from './ArtistItems'
import AnimatedWrapper from "../AnimatedWrapper";
import { connect } from 'react-redux';
import { selectArtist } from '../../store/artists';

class ArtistsComponent extends Component {

  changeSort = (sort) => {
    this.setState({ sort: sort });
  }

  constructor(props) {
    super(props);
    this.state = {
      sort: 'all',
      active: null
    };
  }

  render() {
    const { currentArtist } = this.props;
    const { sort } = this.state;
    return (
      <Fragment>
        <Flex className={currentArtist ? 'fixed' : ''} direction="column" w={1}>
          <ArtistsHeader sort={this.state.sort} onSortClick={this.changeSort} />
          <ArtistItems handleClick={this.setActiveArtist} artist={this.props.artist ? this.props.artist : ''} sort={sort} />
        </Flex>
      </Fragment>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  currentArtist: selectArtist(state, ownProps.artist)
});

const Artists = AnimatedWrapper(connect(mapStateToProps)(ArtistsComponent));
export default Artists;
