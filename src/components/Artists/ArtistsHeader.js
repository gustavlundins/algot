import React, { Component, Fragment } from 'react';
import styled from 'react-emotion'
import MediaQuery from 'react-responsive';
import { Link } from "react-router-dom";
import Icon from '../UI/Icons/Icon';

import { Flex, Box } from '../UI/Base';

const FilterWrapper = styled(Flex)`
  width: 500px;
`;

const Filter = styled('a')`
  margin:0 30px;
  display: block;
  white-space: nowrap;
  text-transform: uppercase;
  cursor: pointer;

  position: relative;
  &:after {
    content: '';
    display: inline-block;
    width: 10px;
    height: 10px;
    margin-left: 10px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    top:2px;
    position: absolute;
    background-color: black;
    transform:scale(0);
    transition:transform .3s ease;
  }

  &.selected {
    &:after {
      transform:scale(1);
    }
  }
`;

const Label = styled(Box)`
  text-align: left;
  font-size: 16px;
`

const Header = styled(Flex)`
  height: 55px;
  min-height: 55px;
  box-sizing: border-box;
  border-bottom:1px solid black;
  background: #FFFFFF;
`;

class ArtistsHeader extends Component {
  render() {
    const { back } = this.props;
    return (
      (
        <Header justify="center" align="center" px={[0,0,2]} w={1} direction="row">
          {back ? (
            <Label w={1}>
              <Box pl={[2,2,25]}>
                <Link to="/artister">
                  <Flex align="center">
                    <Icon title="tillbaka" icon="arrow-left"/>
                    <Box ml={2}>Tillbaka</Box>
                  </Flex>
                </Link>
              </Box>
            </Label>
          ) : (
            <Fragment>
              <Label w={1}>
                <Box pl={[2,2,25]}>
                  Line up
                </Box>
              </Label>
              <MediaQuery query="(min-width: 1224px)">
                <FilterWrapper>
                  <Filter key="1" onClick={() => this.props.onSortClick('all')} className={this.props.sort === 'all' ? "selected" : ""}>Hela helgen</Filter>
                  <Filter key="2" onClick={() => this.props.onSortClick('friday')} className={this.props.sort === 'friday' ? "selected" : ""}>Fredag</Filter>
                  <Filter key="3" onClick={() => this.props.onSortClick('saturday')} className={this.props.sort === 'saturday' ? "selected" : ""}>Lördag</Filter>
                </FilterWrapper>
              </MediaQuery>
            </Fragment>
          )}
        </Header>
      )
    )
  }
}

export default ArtistsHeader;
