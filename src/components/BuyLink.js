import React, { Component } from 'react';
import styled, { keyframes } from 'react-emotion';
import { connect } from 'react-redux';
import { selectBuyLink } from '../store/start';

const shake = keyframes`
  10%, 90% {
    transform: translate3d(-1px, 0, 0) rotate(12deg);
  }

  20%, 80% {
    transform: translate3d(2px, 0, 0) rotate(10deg);
  }

  30%, 50%, 70% {
    transform: translate3d(-4px, 0, 0) rotate(7deg);
  }

  40%, 60% {
    transform: translate3d(4px, 0, 0) rotate(12deg);
  }
`;

const BuyLink = styled('a')`
  width: 120px;
  height: 120px;
  background: #FDD1F2;
  color:black;
  border-radius: 50%;
  padding:20px;
  font-family: Agipo;
  line-height: 19px;
  font-size: 16px;
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content:center;
  position: fixed;
  right: 100px;
  top:263px;
  transform:rotate(12deg);
  z-index: 100;

  span {
    display: block;
  }
  &:hover {
    animation:${shake} 0.82s cubic-bezier(.36,.07,.19,.97) both;
    backface-visibility: hidden;
    perspective: 1000px;
    -webkit-transform-origin: 50%  51%;
  }
`

class BuyLinkComponent extends Component {

  render() {
    return (
      <BuyLink target="_blank" href={this.props.link}>
        <span>Köp</span>
        <span>biljett</span>
        <span>här!</span>
      </BuyLink>
    )
  }

}

const mapStateToProps = state => ({
  link: selectBuyLink(state),
});

export default connect(mapStateToProps)(BuyLinkComponent);
