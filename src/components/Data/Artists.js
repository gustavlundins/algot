export default [
  {
    id: 1,
    title: 'Mystic Lotion',
    from: 'GBG',
    filter: 'friday',
    day: 'Fredag',
    about: 'After high school they moved to Gothenburg and the band fell apart, at the same time as she discovered feminism. Soon she had started a feminist punk band with two friends, playing in front of audiences in Gothenburg, but at the same Åsa had also started to record on her own at home. After having her heart broken she returned to music 16 years old, singing in a local band and self-medicating with beers before every performance.',
    stage: 'Lilla scenen',
    time: '00.00',
    facebook: 'https://facebook.com',
    instagram: 'https://instagram.com',
    image:'/img/shitkid.png'
  },
  {
    id: 2,
    title: 'Hanna Prescott',
    from: 'STHLM',
    filter: 'saturday',
    day: 'Lördag',
    about: 'After high school they moved to Gothenburg and the band fell apart, at the same time as she discovered feminism. Soon she had started a feminist punk band with two friends, playing in front of audiences in Gothenburg, but at the same Åsa had also started to record on her own at home. After having her heart broken she returned to music 16 years old, singing in a local band and self-medicating with beers before every performance.',
    stage: 'Lilla scenen',
    time: '00.00',
    facebook: 'https://facebook.com',
    instagram: 'https://instagram.com',
    image: '/img/hannah_prescott_galaklubb.jpg'
  },
  {
    id: 3,
    title: 'Mystic Lotion',
    from: 'GBG',
    filter: 'saturday',
    day: 'Lördag',
    about: 'After high school they moved to Gothenburg and the band fell apart, at the same time as she discovered feminism. Soon she had started a feminist punk band with two friends, playing in front of audiences in Gothenburg, but at the same Åsa had also started to record on her own at home. After having her heart broken she returned to music 16 years old, singing in a local band and self-medicating with beers before every performance.',
    stage: 'Lilla scenen',
    time: '00.00',
    facebook: 'https://facebook.com',
    instagram: 'https://instagram.com',
    image:'/img/shitkid.png'
  },
  {
    id: 4,
    title: 'Smicker',
    from: 'SKE',
    filter: 'saturday',
    day: 'Lördag',
    about: 'After high school they moved to Gothenburg and the band fell apart, at the same time as she discovered feminism. Soon she had started a feminist punk band with two friends, playing in front of audiences in Gothenburg, but at the same Åsa had also started to record on her own at home. After having her heart broken she returned to music 16 years old, singing in a local band and self-medicating with beers before every performance.',
    stage: 'Lilla scenen',
    time: '00.00',
    facebook: 'https://facebook.com',
    instagram: 'https://instagram.com',
    image:'/img/shitkid.png'
  }
];
