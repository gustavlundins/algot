import React from 'react';
import styled from 'react-emotion'
import Marquee from './Marquee/';

import { Flex } from './UI/Base';
// import Marquee from 'react-smooth-marquee';

const FooterWrapper = styled(Flex)`
  ${'' /* border-bottom:1px solid black; */}
  border-top:1px solid black;
  position: absolute;
  bottom:0;
  left:0;
  right:0;
  width: 100%;
`;

const items = [
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'},
  {title:'ALGOTFESTIVALEN 20—22 JULI 2018', slug:'/'}
];

const Footer = () => (
  <FooterWrapper w={1} direction="column">
    <Marquee direction="left" speed="120" items={items} />
  </FooterWrapper>
);

export default Footer;
