import React, { Component } from 'react';
import styled from 'react-emotion'
import { Flex, Box } from './UI/Base';
import Marquee from './Marquee/';
import Menu from './Menu';
import { Link } from  'react-router-dom';
import { selectNews } from '../store/news';
import { connect } from 'react-redux';

const HeaderLogo = styled(Flex)`
  line-height: 1;
  font-family: 'Opposit';
  border-right:1px solid black;
  width: 65px;
  height: 100%;
  flex-shrink:0;
  text-transform: uppercase;
  @media (min-width: 1024px) {
    width: 105px;
  }
`;
const Hamburger = styled(Flex)`
  border-left:1px solid black;
  cursor: pointer;
  text-align: center;
  width: 65px;
  flex-shrink:0;
  @media (min-width: 1024px) {
    width: 105px;
  }
`;
const LineWrapper = styled(Flex)`
  transition:transform .4s ease;
  &.active {
    transform:rotate(90deg) scale(0.8);
  }
`;

const Line = styled(Flex)`
  width: 25px;
  text-align: center;
  height: 1px;
  background: black;
  margin: 3px auto;
`;

const Headerwrapper = styled(Flex)`
  border-top:1px solid black;
  border-bottom:1px solid black;
  background: white;
  position: fixed;
  width: 100%;
  top:0;
  left:0;
  z-index: 100;

  transition: background .1s linear;

  &.white {
    background:white;
  }
`

class Header extends Component {

  static defaultProps = {
    news: []
  };

  constructor (props) {
    super(props)
    this.state = {
      background: false,
      menuActive: false,
    }
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
    document.addEventListener('click', this.handleOutsideClick, false);
  }

  toggleMenu = (background) => {
    this.setState({
      background: background,
      menuActive: !this.state.menuActive
    })
  }

  handleOutsideClick = (e) => {
    if (this.node.contains(e.target)) {
      return;
    }
    if(this.state.menuActive){
      this.resetMenu();
    }
  }

  resetMenu = () => {
    this.setState({
      background: null,
      menuActive: false
    })
  }

  render() {
    return (
      <Box>
        <div ref={node => { this.node = node; }}>
          <Headerwrapper direction="row" className={(this.state.menuActive || this.state.background) ? "white" : ""}>
            <Link to="/" onClick={this.resetMenu}>
            <HeaderLogo fontSize={[5,5,46]} align="center" justify="center">
              <span>a</span>
            </HeaderLogo>
            </Link>
            <Marquee items={this.props.news} direction="left" speed={100} bx bb={false}/>
            <Hamburger justify="center" align="center" direction="column" onClick={this.toggleMenu}>
              <LineWrapper className={this.state.menuActive ? 'active' : ''} align="center" direction="column">
                <Line></Line>
                <Line></Line>
                <Line></Line>
              </LineWrapper>
            </Hamburger>
            <Menu isActive={this.state.menuActive} onClick={this.toggleMenu} />
          </Headerwrapper>
        </div>
      </Box>
    )
  }
}
const mapStateToProps = state => ({
  news: selectNews(state),
});

export default connect(mapStateToProps)(Header);
