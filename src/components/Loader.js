import React from 'react';
import styled, { keyframes } from 'react-emotion'

import { Flex, Box } from './UI/Base';

const loadCharacter = keyframes`
	0% {
		transform: translateY(0%);
	}

  100% {
    transform: translateY(-100%);
  }
`
const loadWrapper = keyframes`
	0% {
		transform: translateY(0%);
	}
  75% {
    transform: translateY(0%);
  }
	95% {
		opacity: 1;
	}
  100% {
    transform: translateY(-100%);
		opacity:0;
  }
`

const LoaderWrapper = styled(Flex)`
  background: #D4AAA8;
  position: fixed;
  top:0;
  left:0;
  right: 0;
  bottom:0;
  color:white;
  font-size: 90px;
	z-index: 1000;
  text-align: center;
  animation: ${loadWrapper} 2.5s ease;
	animation-delay: 150ms;
  animation-fill-mode: forwards;
  transform-origin: center bottom;
`;
const LoadingCharacter = styled(Box)`
  display: inline-block;
  position: relative;
	font-family: Opposit;
	font-size: 80px;
`;
const Overlay = styled(Box)`
  position: absolute;
  width: 100%;
  height:100%;
  background: rgba(212,170,168,0.5);
  top:0;
  left:0;
  transform: translateY(0%);
  animation: ${loadCharacter} 1.6s ease;
  animation-fill-mode: forwards;
	animation-delay: 600ms;
  transform-origin: center bottom;
`;

const Image = styled('img')`
	max-width: 90%;
`

const Loader = () => (
  <LoaderWrapper justify="center" align="center">
    <LoadingCharacter>
      <Image src={'/algot.svg'} alt={'algot'} />
      <Overlay></Overlay>
    </LoadingCharacter>
  </LoaderWrapper>
);

export default Loader;
