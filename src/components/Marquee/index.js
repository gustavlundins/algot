import React, { Component, Fragment } from "react";
import styled, { css, keyframes } from "react-emotion";
import { Box, Flex } from "../UI/Base";
import { Link } from "react-router-dom";
import Loading from "../UI/Loading";

const scroll = keyframes`
	0% {
		-webkit-transform: translateX(-100%);
		-ms-transform: translateX(-100%);
		transform: translateX(-100%);
	}
	100% {
		-webkit-transform: translateX(100%);
		-ms-transform: translateX(100%);
		transform: translateX(100%);
	}
`;
const scroll2 = keyframes`
  0% {
		-webkit-transform: translateX(-200%);
		-ms-transform: translateX(-200%);
		transform: translateX(-200%);
	}
  100% {
		-webkit-transform: translateX(0%);
		-ms-transform: translateX(0%);
		transform: translateX(0%);
	}
`;
const scroll3 = keyframes`
  0% {
		-webkit-transform: translateX(100%);
		-ms-transform: translateX(100%);
		transform: translateX(100%);
	}
  100% {
		-webkit-transform: translateX(-100%);
		-ms-transform: translateX(-100%);
		transform: translateX(-100%);
	}
`;
const scroll4 = keyframes`
  0% {
		-webkit-transform: translateX(0%);
		-ms-transform: translateX(0%);
		transform: translateX(0%);
	}
  100% {
		-webkit-transform: translateX(-200%);
		-ms-transform: translateX(-200%);
		transform: translateX(-200%);
	}
`;

const Wrapper = styled(Flex)`
  width: 100%;
  overflow: hidden;
  height: 60px;
  white-space: nowrap;
  &.border-bottom {
    border-bottom: 1px solid black;
  }
`;

const Items = styled(Box)`
  display: inline-block;
  position: relative;
  animation: ${scroll} ${props => props.speed}s linear infinite;
  animation-delay: -${props => props.speed / 2}s;
	animation-play-state: running;
`;
const Items2 = styled(Items)`
  transform: translateX(-200%);
  animation: ${scroll2} ${props => props.speed}s linear infinite;
`;
const Items3 = styled(Items)`
  animation: ${scroll3} ${props => props.speed}s linear infinite;
  animation-delay: -${props => props.speed / 2}s;
`;
const Items4 = styled(Items)`
  transform: translateX(-200%);
  animation: ${scroll4} ${props => props.speed}s linear infinite;
`;
const Item = styled(Link)`
  line-height: 1;
  font-family: Agipo;
  font-size: 16px;
  display: inline-block;
  padding: 0 30px;
  vertical-align: middle;
  text-transform: uppercase;
  line-height: 1;
  height: 60px;
  line-height: 60px;
	&:hover {
		background:#000000;
		color:#ffffff;
	}
`;
const ItemExternal = styled("a")`
  line-height: 1;
  font-family: Agipo;
  font-size: 16px;
  display: inline-block;
  padding: 0 30px;
  vertical-align: middle;
  text-transform: uppercase;
  line-height: 1;
  height: 60px;
  line-height: 60px;
	&:hover {
		background:#000000;
		color:#ffffff;
	}
`;
const BorderRight = css`
  border-right: 1px solid black;
`;

const ItemBlock = (props) => {
	if(props.internal) {
		return (
			<Item
				className={props.className}
				to={props.to}
			>
				{props.title}
			</Item>
		)
	} else {
		return (
			<ItemExternal
				className={props.className}
				href={props.to}
				target="_blank"
			>
				{props.title}
			</ItemExternal>
		)
	}
};

class Marquee extends Component {
  static defaultProps = {
    items: [],
    speed: 60,
    direction: null,
    bx: null,
    bb: true
  };

  render() {
    return (
			<Fragment>
				{this.props.items.length > 0 && (
					<Wrapper className={this.props.bb ? "border-bottom" : ""} align="center">
		        {this.props.items.length < 1 && <Loading />}
		        {this.props.direction ? (
		          <Fragment>
		            <Items3 className="items" speed={this.props.speed}>
		              {this.props.items.map((item, index) => (
		                <ItemBlock
		                  className={this.props.bx ? BorderRight : ""}
		                  to={item.slug ? item.slug : item.path}
		                  key={index}
											internal={item.internal !== undefined ? item.internal : true}
											title={item.from ? item.title + ' (' + item.from + ')' : item.title}
		                />
		              ))}
		              {this.props.items.length < 7 &&
		                this.props.items.map((item, index) => (
											<ItemBlock
			                  className={this.props.bx ? BorderRight : ""}
			                  to={item.slug ? item.slug : item.path}
			                  key={index}
												internal={item.internal !== undefined ? item.internal : true}
												title={item.from ? item.title + ' (' + item.from + ')' : item.title}
			                />
		                ))}
		            </Items3>
		            <Items4 className="items" speed={this.props.speed}>
		              {this.props.items.map((item, index) => (
										<ItemBlock
		                  className={this.props.bx ? BorderRight : ""}
		                  to={item.slug ? item.slug : item.path}
		                  key={index}
											internal={item.internal !== undefined ? item.internal : true}
											title={item.from ? item.title + ' (' + item.from + ')' : item.title}
		                />
		              ))}
		              {this.props.items.length < 7 &&
		                this.props.items.map((item, index) => (
											<ItemBlock
			                  className={this.props.bx ? BorderRight : ""}
			                  to={item.slug ? item.slug : item.path}
												internal={item.internal !== undefined ? item.internal : true}
			                  key={index}
												title={item.from ? item.title + ' (' + item.from + ')' : item.title}
			                />
		                ))}
		            </Items4>
		          </Fragment>
		        ) : (
		          <Fragment>
		            <Items className="items" speed={this.props.speed}>
		              {this.props.items.map((item, index) => (
										<ItemBlock
											className={this.props.bx ? BorderRight : ""}
		                  to={item.slug ? item.slug : item.path}
		                  key={index}
											internal={item.internal !== undefined ? item.internal : true}
											title={item.from ? item.title + ' (' + item.from + ')' : item.title}
										/>
		              ))}
		              {this.props.items.length < 7 &&
		                this.props.items.map((item, index) => (
											<ItemBlock
												className={this.props.bx ? BorderRight : ""}
												to={item.slug ? item.slug : item.path}
												key={index}
												internal={item.internal !== undefined ? item.internal : true}
												title={item.from ? item.title + ' (' + item.from + ')' : item.title}
											/>
		                ))}
		            </Items>
		            <Items2 className="items" speed={this.props.speed}>
		              {this.props.items.map((item, index) => (
										<ItemBlock
											className={this.props.bx ? BorderRight : ""}
		                  to={item.slug ? item.slug : item.path}
		                  key={index}
											internal={item.internal !== undefined ? item.internal : true}
											title={item.from ? item.title + ' (' + item.from + ')' : item.title}
										/>
		              ))}
		              {this.props.items.length < 7 &&
		                this.props.items.map((item, index) => (
											<ItemBlock
												className={this.props.bx ? BorderRight : ""}
			                  to={item.slug ? item.slug : item.path}
			                  key={index}
												internal={item.internal !== undefined ? item.internal : true}
												title={item.from ? item.title + ' (' + item.from + ')' : item.title}
											/>
		                ))}
		            </Items2>
		          </Fragment>
		        )}
		      </Wrapper>
				)}
			</Fragment>
    );
  }
}

export default Marquee;
