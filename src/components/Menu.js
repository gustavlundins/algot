import React, { Component } from "react";
import styled from "react-emotion";
import { Flex, Box } from "./UI/Base";
import { Link } from "react-router-dom";
import Icon from "./UI/Icons/Icon";

const MenuItem = styled(Link)`
  display: block;
  text-align: left;
  height: 55px;
  line-height: 55px;
  border-bottom: 1px solid black;
  background: white;
  ${'' /* padding: 0 40px; */}
  color: black;
  font-size: 24px;
  text-decoration: none;
  span {
    opacity: 0;
    transition: opacity 0.1s linear;
  }
  &.title {
    font-size: 16px;
    font-family: Agipo;
  }
  &.active {
    span {
      opacity: 1;
    }
  }
  .SVGInline {
    opacity: 0 !important;
  }
  &:hover  {
    .SVGInline {
      opacity: 1 !important;
    }
  }
`;
const ExtItem = styled('a')`
display: block;
text-align: left;
height: 55px;
line-height: 55px;
border-bottom: 1px solid black;
background: white;
color: black;
font-size: 24px;
text-decoration: none;
span {
  opacity: 0;
  transition: opacity 0.1s linear;
}
&.title {
  font-size: 16px;
  font-family: Agipo;
}
&.active {
  span {
    opacity: 1;
  }
}
.SVGInline {
  opacity: 0 !important;
}
&:hover  {
  .SVGInline {
    opacity: 1 !important;
  }
}
`
const IconBox = styled(Box)`
  display: inline-block;
  height: 43px;
  vertical-align: middle;

`

const MenuWrapper = styled(Flex)`
  position: absolute;
  transition: all 0.4s ease;
  z-index: 10;
  top: calc(100% + 1px);
  transform-origin: top left;
  transform: scaleY(0);
  &.active {
    transform: scaleY(1);
  }
`;

class Menu extends Component {
  static defaultProps = {
    isActive: false
  };

  handleClick = (background, e) => {
    this.props.onClick(background);
  };

  render() {
    return (
      <MenuWrapper
        w={1}
        className={this.props.isActive ? "active" : ""}
        direction="column"
      >
        <MenuItem
          className={this.props.isActive ? "active title" : "title"}
          to="/"
          onClick={e => {
            e.preventDefault();
          }}
        >
          <Box px={[16,16,16,40]}>
            <span>Meny</span>
          </Box>
        </MenuItem>
        {/* <MenuItem
          className={this.props.isActive ? "active" : ""}
          to="/artister"
          onClick={() => this.handleClick(true)}
        >
          <span>Line up</span>
          <IconBox ml={4}>
            <Icon icon={"pil-light"} height={"28"} width={"36"} />
          </IconBox>
        </MenuItem> */}
        <ExtItem
          className={this.props.isActive ? "active" : ""}
          href="https://billetto.se/en/e/algotfestivalen-2018-tickets-271420"
          onClick={() => this.handleClick(false)}
          target={'_blank'}
        >
          <Box px={[16,16,16,40]}>
            <span>Köp biljetter</span>
            <IconBox ml={4}>
              <Icon icon={"pil-light"} height={"28"} width={"36"} />
            </IconBox>
          </Box>
        </ExtItem>
        <MenuItem
          className={this.props.isActive ? "active" : ""}
          to="/artister"
          onClick={() => this.handleClick(false)}
        >
          <Box px={[16,16,16,40]}>
            <span>Artister</span>
            <IconBox ml={4}>
              <Icon icon={"pil-light"} height={"28"} width={"36"} />
            </IconBox>
          </Box>
        </MenuItem>
        <MenuItem
          className={this.props.isActive ? "active" : ""}
          to="/om-festivalen"
          onClick={() => this.handleClick(false)}
        >
          <Box px={[16,16,16,40]}>
            <span>Om festivalen</span>
            <IconBox ml={4}>
              <Icon icon={"pil-light"} height={"28"} width={"36"} />
            </IconBox>
          </Box>
        </MenuItem>
        <MenuItem
          className={this.props.isActive ? "active" : ""}
          to="/om-festivalen"
          onClick={() => this.handleClick(false)}
        >
          <Box px={[16,16,16,40]}>
            <span>Bli volontär</span>
            <IconBox ml={4}>
              <Icon icon={"pil-light"} height={"28"} width={"36"} />
            </IconBox>
          </Box>
        </MenuItem>
        <MenuItem
          className={this.props.isActive ? "active" : ""}
          to="/om-festivalen"
          onClick={() => this.handleClick(false)}
        >
          <Box px={[16,16,16,40]}>
            <span>Kontakt</span>
            <IconBox ml={4}>
              <Icon icon={"pil-light"} height={"28"} width={"36"} />
            </IconBox>
          </Box>
        </MenuItem>
      </MenuWrapper>
    );
  }
}

export default Menu;
