import { Component } from 'react';
import { selectArtists } from '../store/artists';
import { fetchArtists } from '../action/artists';
import { fetchStartData } from '../action/start';
import { fetchNewsData } from '../action/news';
import { fetchAboutData } from '../action/about';
import { connect } from 'react-redux';

class Prefetch extends Component {

  componentDidMount(){
    this.props.onFetchArtists();
    this.props.onFetchStart();
    this.props.onfetchNewsData();
    this.props.onfetchAboutData();

  }

  render() {
    return true;
  }
}

const mapStateToProps = state => ({
  artists: selectArtists(state),
});
const mapDispatchToProps = dispatch => ({
  onFetchArtists: () => {
    dispatch(fetchArtists());
  },
  onFetchStart: () => {
    dispatch(fetchStartData());
  },
  onfetchNewsData: () => {
    dispatch(fetchNewsData());
  },
  onfetchAboutData: () => {
    dispatch(fetchAboutData());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Prefetch);
