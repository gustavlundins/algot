import { Component } from 'react';
import { withRouter } from 'react-router';
import scrollToComponent from 'react-scroll-to-component';

class ScrollToTop extends Component {
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      scrollToComponent(this.props.position, { offset: 0, align: 'top', duration: 250, ease:'inOutQuint'});
    }
  }
  render() {
    return this.props.children;
  }
}

export default withRouter(ScrollToTop);
