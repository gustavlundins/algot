import React, { Component } from 'react';
import styled from 'react-emotion'
import { Flex, Box } from '../UI/Base';
import { Link } from  'react-router-dom';
import BorderBottom from '../Helpers';
import MediaQuery from 'react-responsive';

const ArtistImage = styled('img')`
	width: 100%;
	height: 100%;
`

const StyledLink = styled(Link)`
	height: 100%;
	width: 100%;
	position: relative;
	transition: background 50ms linear;
	&:hover {
		background:#ccc;
	}
`
const StyledBox = styled(Box)`
	position: relative;
	top:50%;
	transform: translateY(-50%)
`

class Artists extends Component {
  render() {
    return (
      <Flex className={BorderBottom} w={1} direction="column">
				<MediaQuery query="(max-width: 1224px)">
					{this.props.artists.length > 0 && (
						<Flex wrap="wrap" w={1} direction={['column','row']} fontSize={[24,80]}>
							<Flex w={[1,1/2]}>
								<ArtistImage src={this.props.artists[0].thumbnail} />
							</Flex>
							<Flex w={[1,1/2]} justify="center" align="center">
								<Link to={this.props.artists[0].slug}>
									<Box px={2} py={4}>
										{this.props.artists[0].title}{ }
										({this.props.artists[0].from})
									</Box>
								</Link>
							</Flex>
							<Flex w={[1,1/2]}>
								<ArtistImage src={this.props.artists[1].thumbnail} />
							</Flex>
							<Flex w={[1,1/2]} justify="center" align="center">
								<Link to={this.props.artists[1].slug}>
									<Box px={2} py={4}>
										{this.props.artists[1].title}{ }
										({this.props.artists[1].from})
									</Box>
								</Link>
							</Flex>
						</Flex>
					)}
		    </MediaQuery>
				<MediaQuery query="(min-width: 1224px)">
					{this.props.artists.length > 0 && (
						<Flex wrap="wrap" w={1} direction={['column','row']} fontSize={[24,80]}>
							<Flex w={[1,1/2]} justify="center" align="center">
								<StyledLink to={this.props.artists[0].slug}>
									<StyledBox p={4}>
										{this.props.artists[0].title}{ }
										({this.props.artists[0].from})
									</StyledBox>
								</StyledLink>
							</Flex>
							<Flex w={[1,1/2]}>
								<Link to={this.props.artists[1].slug}>
									<ArtistImage src={this.props.artists[1].thumbnail} />
								</Link>
							</Flex>
							<Flex w={[1,1/2]}>
								<Link to={this.props.artists[0].slug}>
									<ArtistImage src={this.props.artists[0].thumbnail} />
								</Link>
							</Flex>
							<Flex w={[1,1/2]} justify="center" align="center">
								<StyledLink to={this.props.artists[1].slug}>
									<StyledBox p={4}>
										{this.props.artists[1].title}{ }
										({this.props.artists[1].from})
									</StyledBox>
								</StyledLink>
							</Flex>
						</Flex>
					)}
		    </MediaQuery>
      </Flex>
    )
  }
}

export default Artists;
