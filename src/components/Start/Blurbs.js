import React, { Component, Fragment } from 'react';
import styled from 'react-emotion'
import { Flex, Box, Text } from '../UI/Base';
import BorderBottom from '../Helpers';

const StyledFlex = styled(Flex)`
  position: relative;
  overflow: hidden;
  &:after {
    content: "";
    display: block;
    padding-bottom: 46.25%;
  }

`
const Column = (props, children) => {
  return (
  <StyledFlex {...props} w={[1,null,1/2]}>
    {props.children}
  </StyledFlex>
)}

const Titles = styled(Flex)`
  div {
    -webkit-transform:scale(1);
    transition: transform 350ms ease;
    will-change: transform, color;
    font-family: 'Opposit';
  }
  div:nth-child(1) {
    transform:translateY(100%);
  }
  div:nth-child(3) {
    transform:translateY(-100%);
  }
  &:hover {
    div:nth-child(1) {
      transform:translateY(0%);
    }
    div:nth-child(3) {
      transform:translateY(0%);
    }
  }
`

const Year = styled(Flex)`
  div {
    font-weight: 500;
    -webkit-transform:scale(1);
    transform:scaleY(1);
    transition: transform 350ms ease;
    will-change: transform;
    transform-origin:0 0;
  }
  &:hover {
    div {
      -webkit-transform:scaleY(2.6) translateY(-27%);
      transform:scaleY(2.6) translateY(-27%);
    }
  }
`;

const Monarch = styled(Text)`
  font-family: 'Monarch';
  display: inline-block;

  &.arrow {
    transition:transform .1s ease;
    transform: translateX(0);
  }
`
const Arrow = styled(Flex)`
  font-family: 'Monarch';
  &:hover {
    .arrow {
      transform:translateX(20px);
    }
  }
`

const Background = styled(Flex)`
  background: url(${props => props.backgroundImage});
  background-size: cover;
  background-position: center center;
`

class Blurbs extends Component {
  render() {
    return (
      <Flex className={BorderBottom} w={1} direction="column">
				{this.props.artists.length > 0 && (
					<Fragment>
            <Flex wrap="wrap" w={1} direction={['column','row']}>
  						<Column>
                <Titles w={1} justify={'center'} align={'center'} fontSize={[48,'6vw']} column>
                  <Box><Text>ALGOT</Text></Box>
                  <Box><Text>ALGOT</Text></Box>
                  <Box><Text>ALGOT</Text></Box>
                </Titles>
              </Column>
              <Column>
                <Year bg={'#D4AAA8'} lineHeight={'1em'} w={1} justify={'center'} align={'center'} fontSize={[56,'9vw']} column>
                  <Box><Text>2018</Text></Box>
                </Year>
              </Column>
  					</Flex>
            <Flex wrap="wrap" w={1} direction={['column','row']}>
              <Column bg={'black'} color={'white'}>
                <Arrow w={1} justify={'center'} align={'center'} fontSize={[48,'4vw']} column>
                  <Flex>
                    <Monarch>Koralldjur</Monarch>
                    <Monarch className={'arrow'} ml={2}>&rarr;</Monarch>
                  </Flex>
                </Arrow>
              </Column>
              <Background w={[1,null,1/2]} backgroundImage={'/img/colors.png'}>
              </Background>
            </Flex>
          </Fragment>
				)}
      </Flex>
    )
  }
}

export default Blurbs;
