import React, { Component, Fragment } from 'react';
import styled from 'react-emotion'
import { Flex, Box } from '../UI/Base';
import Icon from '../UI/Icons/Icon';
import MediaQuery from 'react-responsive';
const Link = styled('a')`
  width: 100%;
  text-decoration: none;
  transition: opacity 100ms linear;
  &:hover {
    background:#000000;
    color:#FFFFFF;

    polygon {
      fill:#FFFFFF !important;
      stroke:#FFFFFF;
    }
  }
`
const Center = styled(Flex)`
  border-left: 1px solid black;
  border-right: 1px solid black;
`;
const Bottom = styled(Flex)`
  border-bottom: 1px solid black;
`;

class Social extends Component {
  render() {

    return (
      <Flex w={1} direction={['column','column','row']}>
        {this.props.social && (
          <Fragment>
            <MediaQuery query="(max-width: 768px)">
              <Bottom w={[1,1,1/3]} fontSize={[2,2,4]} justify="center" align="center">
                <Link className="agipo" href={this.props.social.instagram} target="_blank">
                  <Box py={[18,62]}>
                    Instagram <Icon icon="pil-light-up-right" height={'16'} width={'16'} title={'Pil'} />
                  </Box>
                </Link>
              </Bottom>
              <Bottom w={[1,1,1/3]} fontSize={[2,2,4]} justify="center" align="center">
                <Link className="agipo" href={this.props.social.facebook} target="_blank">
                  <Box py={[18,62]}>
                    Facebook <Icon icon="pil-light-up-right" height={'16'} width={'16'} title={'Pil'} />
                  </Box>
                </Link>
              </Bottom>
              <Flex w={[1,1,1/3]} fontSize={[2,2,4]} justify="center" align="center">
                <Link className="agipo" href={this.props.social.buy} target="_blank">
                  <Box py={[18,62]}>
                    Köp biljett <Icon icon="pil-light-up-right" height={'16'} width={'16'} title={'Pil'} />
                  </Box>
                </Link>
              </Flex>
            </MediaQuery>

            <MediaQuery query="(min-width: 768px)">
              <Flex w={[1,1/3]} fontSize={[2,2,4]} justify="center" align="center">
                <Link className="agipo" href={this.props.social.instagram} target="_blank">
                  <Box py={[18,168]}>
                    Instagram <Icon icon="pil-light-up-right" height={'18'} width={'18'} title={'Pil'} />
                  </Box>
                </Link>
              </Flex>
              <Center w={[1,1/3]} fontSize={[2,2,4]} justify="center" align="center">
                <Link className="agipo" href={this.props.social.facebook} target="_blank">
                  <Box py={[18,168]}>
                    Facebook <Icon icon="pil-light-up-right" height={'18'} width={'18'} title={'Pil'} />
                  </Box>
                </Link>
              </Center>
              <Flex w={[1,1/3]} fontSize={[2,2,4]} justify="center" align="center">
                <Link className="agipo" href={this.props.social.buy} target="_blank">
                  <Box py={[18,168]}>
                    Köp biljett <Icon icon="pil-light-up-right" height={'18'} width={'18'} title={'Pil'} />
                  </Box>
                </Link>
              </Flex>
            </MediaQuery>
          </Fragment>
        )}
      </Flex>
    )
  }
}

export default Social;
