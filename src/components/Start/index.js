import React, { Component } from "react";
import styled, { css } from "react-emotion";
import { Box, Flex, Text } from "../UI/Base";
import Video from "react-background-video-player";
import Marquee from "../Marquee";
import { Link } from "react-router-dom";
// import Artists from './Artists';
import Blurbs from "./Blurbs";
import Social from "./Social";
import AnimatedWrapper from "../AnimatedWrapper";
import scrollToComponent from "react-scroll-to-component";
import Icon from "../UI/Icons/Icon";
import MediaQuery from "react-responsive";
import { connect } from "react-redux";
import { selectArtists } from "../../store/artists";
import { selectStartInfo, selectSocialInfo } from "../../store/start";

const Background = css(`
	video {
		left:50% !important;
		top:50% !important;
		height: auto !important;
    min-height: 100%;
    min-width: 100%;
		transform:translateX(-50%)translateY(-50%);
		z-index:5;
	}
`);
const BorderBottom = css(`
	border-bottom:1px solid black;
`);
const StyledText = styled(Text)`
  max-width: ${props => props.max}px;
  display: inline-block;
`;
const Column = styled(Flex)`
  position: relative;
  overflow: hidden;
`;
const RowBackground = styled("div")`
  background: ${props => props.background};
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;
  opacity: 0;
  transition: opacity 100ms linear;
  overflow: hidden;
  span {
    transition: transform 0.05s ease-in;
  }
  &:hover {
    span {
      transform: skew(-12deg);
    }
  }
`;
const Row = styled(Flex)`
  font-family: Opposit;
  font-weight: 500;
  position: relative;
  border-bottom: 1px solid black;
  background: ${props => props.background};
  .text {
    transition: transform 0.05s ease-in;
  }
	svg {
		transition: transform 0.05s ease-in;
	}
  &:hover {
    .text {
      transform: skew(-12deg);
    }
		svg {
			transform: translateY(10px);
		}
  }
`;
const RowFlex = styled(Flex)`
  position: relative;
  a {
    transition: transform 0.05s ease-in;
  }
  &:hover {
    a {
      transform: skew(-12deg);
    }
  }
`;
const RowLink = styled(Link)`
  width: 100%;
  &.small {
    font-family: "Agipo";
  }
`;
const ExtLink = styled("a")`
  width: 100%;
  &.small {
    font-family: "Agipo";
  }
`;
const BigLink = styled(RowLink)`
  padding: 78px 20px;
`;

const Title = styled(Flex)`
  position: absolute;
  width: 100%;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: 10;
`;

const Img = styled("img")`
  max-width: 90%;
`;

class StartComponent extends Component {
  static defaultProps = {
    artists: []
  };

  render() {
    return (
      <Flex className="page" w={1} direction="column">
        {this.props.info.info1.path && (
          <Flex w={1} direction={["column", "column", "row"]}>
            <Column
              pt={["100%", "100%", 0]}
              className={BorderBottom}
              w={[1, 1, 1 / 2]}
            >
              <Video
                src={"/video/algot-bg.mp4"}
                containerWidth={20}
                containerHeight={20}
                muted={true}
                autoplay={true}
                disableBackgroundCover={false}
                className={Background}
              />
              <Title align={"center"} justify={"space-around"} is="h1">
                <Box>
                  <Img src={"/algot.svg"} alt={"algot"} />
                </Box>
              </Title>
            </Column>
            <Column w={[1, 1, 1 / 2]} direction="column">
              <Row justify="center" fontSize={[42, 54]}>
                {this.props.info.info1.internal ? (
                  <RowLink
                    to={
                      this.props.info.info1.path
                        ? this.props.info.info1.path
                        : ""
                    }
                  >
                    <Box w={1} py={[4, 4, 4]} px={[3, 3, 4]}>
                      <StyledText className="text" max="480">
                        {this.props.info.info1.text}
                      </StyledText>
                    </Box>
                  </RowLink>
                ) : (
                  <ExtLink
                    target="_blank"
                    href={
                      this.props.info.info1.path
                        ? this.props.info.info1.path
                        : ""
                    }
                  >
                    <Box w={1} py={[4, 4, 4]} px={[3, 3, 4]}>
                      <StyledText className="text" max="480">
                        {this.props.info.info1.text}
                      </StyledText>
                    </Box>
                  </ExtLink>
                )}
              </Row>
              <Row justify="center" fontSize={[42, 54]}>
                {this.props.info.info2.internal ? (
                  <RowLink
                    to={
                      this.props.info.info2.path
                        ? this.props.info.info2.path
                        : ""
                    }
                  >
                    <Box w={1} py={[4, 4, 4]} px={[3, 3, 4]}>
                      <StyledText className="text" max="480">
                        {this.props.info.info2.text}
                      </StyledText>
                    </Box>
                  </RowLink>
                ) : (
                  <ExtLink
                    target="_blank"
                    href={
                      this.props.info.info2.path
                        ? this.props.info.info2.path
                        : ""
                    }
                  >
                    <Box w={1} py={[4, 4, 4]} px={[3, 3, 4]}>
                      <StyledText className="text" max="480">
                        {this.props.info.info2.text}
                      </StyledText>
                    </Box>
                  </ExtLink>
                )}
              </Row>
              <MediaQuery query="(min-width: 768px)">
                <Row justify="center" fontSize={[24, 24]}>
                  <RowLink
                    className="small"
                    onClick={() =>
                      scrollToComponent(this.Content, {
                        offset: -61,
                        align: "top",
                        duration: 1250,
                        ease: "inOutQuint"
                      })
                    }
                    to="#"
                  >
                    <Box w={1} p={[4, 4, 4]}>
                      <Box mb={2}>
                        Läs mer<br />
                      </Box>
                      <Icon
                        icon="pil-light-ner"
                        mt={3}
                        height={"32"}
                        width={"32"}
                        title={"Pil"}
                      />
                    </Box>
                  </RowLink>
                </Row>
              </MediaQuery>
            </Column>
          </Flex>
        )}
        <Flex w={1}>
          <Marquee items={this.props.artists} />
        </Flex>
        <MediaQuery query="(min-width: 768px)">
          <Blurbs artists={this.props.artists.slice(0, 2)} />
        </MediaQuery>
				{/* <Flex w={1}>
					<Marquee items={this.props.artists} />
				</Flex>
        <Artists artists={this.props.artists.slice(2,4)} /> */}
        <RowFlex
          ref={RowFlex => {
            this.Content = RowFlex;
          }}
          className={BorderBottom}
          w={1}
          fontSize={[32, 40]}
          justify="space-around"
          align="center"
        >
          <BigLink className="opposit" to="/om-festivalen">
            Om Algotfestivalen
          </BigLink>
          <RowBackground background={"#D4AAA8"} className="RowBackground" />
        </RowFlex>
        <Social social={this.props.social} />
      </Flex>
    );
  }
}

const mapStateToProps = state => ({
  artists: selectArtists(state),
  info: selectStartInfo(state),
  social: selectSocialInfo(state)
});

const Start = AnimatedWrapper(connect(mapStateToProps)(StartComponent));
export default Start;
