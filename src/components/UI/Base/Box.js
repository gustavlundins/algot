import styled from 'react-emotion';
import tag from 'tag-hoc';
import removeProps from '../../../lib/removeProps';
import {
  space,
  width,
  fontSize,
  color,
  flex,
  responsiveStyle
} from 'styled-system';

// Remove props from rendered DOMelement, but allow them to
// pass through to styled-system
const Tag = tag(removeProps);

// Default to a div
const Base = Tag('div');

const Box = styled(Base)`
  ${space} ${width} ${fontSize} ${color} ${responsiveStyle} ${flex};
`;
Box.displayName = 'Box';

export default Box;
