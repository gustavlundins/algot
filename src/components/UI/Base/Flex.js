import styled from 'react-emotion';
import { responsiveStyle } from 'styled-system';
import Box from './Box';

const wrap = responsiveStyle('flex-wrap', 'wrap', 'wrap');
const direction = responsiveStyle('flex-direction', 'direction');
const align = props => responsiveStyle('align-items', 'align');
const justify = props => responsiveStyle('justify-content', 'justify');
const column = props => (props.column ? `flex-direction:column;` : null);
const flex = props => responsiveStyle('flex', 'flex');

const Flex = styled(Box)`
  display: flex;
  ${wrap};
  ${column};
  ${direction};
  ${align};
  ${justify};
  ${flex};
`;
Flex.displayName = 'Flex';

export default Flex;
