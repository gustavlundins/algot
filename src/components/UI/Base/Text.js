import styled from 'react-emotion';
import tag from 'tag-hoc';
import removeProps from '../../../lib/removeProps';
import {
  space,
  fontSize,
  color,
  textAlign,
  lineHeight,
  fontWeight,
  letterSpacing,
  width
} from 'styled-system';

// Remove props from rendered DOMelement, but allow them to
// pass through to styled-system
const Tag = tag(removeProps);

// Default to a span
const Base = Tag('span');

const Text = styled(Base)`
  ${space} ${width} ${fontSize} ${color} ${textAlign} ${lineHeight} ${fontWeight} ${letterSpacing};
`;
Text.displayName = 'Text';

export default Text;
