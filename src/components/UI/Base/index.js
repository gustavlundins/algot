import Box from './Box';
import styled from 'react-emotion';
export { default as Flex } from './Flex';
export { default as Box } from './Box';
export { default as Text } from './Text';
const InlineBox = styled(Box)`
  display: inline-block;
`
export {InlineBox};
