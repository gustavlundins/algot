import styled from 'react-emotion';
import withProps from 'recompose/withProps';
import { Box } from './Base';

/*
  Used to wrap content to ensure it adheres to the grid
*/
const Container = withProps({
  mx: 'auto',
  px: 2,
  width: 1
})(styled(Box)`
  max-width: 82rem;
  color: #363636;
  label: container;
`);

export default Container;
