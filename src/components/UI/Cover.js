import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'react-emotion';
import inServer from '../../lib/inServer';

const Cover = styled('div')`
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: fixed;
  background: rgba(0, 0, 0, 0.2);
  z-index: 5;
`;

export default () => {
  // Return a portal only if this isn't rendered on the server
  return (
    !inServer &&
    ReactDOM.createPortal(<Cover />, document.getElementById('root'))
  );
};
