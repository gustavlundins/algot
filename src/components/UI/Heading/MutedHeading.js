import React from 'react';

import { Text } from '../Base';

/**
 * Default to an h2, but encourage use of 'is' prop to
 * define heading type.
 *
 * Passes through all styled-system props to Text
 */
export default ({ children, is = 'h2', ...rest }) => (
  <Text fontSize={3} fontWeight={0} color="gray.5" is={is} {...rest}>
    {children}
  </Text>
);
