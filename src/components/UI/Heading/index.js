export { default as Heading } from './Heading';
export { default as LargeHeading } from './LargeHeading';
export { default as MutedHeading } from './MutedHeading';
