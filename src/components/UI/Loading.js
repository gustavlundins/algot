import React from 'react';
import Container from './Container';
import styled, { keyframes } from 'react-emotion';
import Icon from './Icons/Icon';

const rotate = keyframes`
  0%, {
    transform: rotate(0);
    opacity: 1;
  }
  50% {
    opacity: 0.5;
  }
  100% {
    transform: rotate(180deg);
    opacity: 1;
  }
`;

const RotatingIcon = styled(Icon)`
  animation: ${rotate} 1.5s linear infinite;
`;

export default () => (
  <Container my={3} align="center">
    <RotatingIcon icon="kryss" title="Loading" width={24} height={24} />
  </Container>
);
