import React, { Component } from 'react';
import { selectArtists } from '../store/artists';
import { fetchArtists } from '../action/artists';

import { connect } from 'react-redux';

class PreFetchData extends Component {

  render () {
    return true;
  }
}

const mapStateToProps = state => ({
  artists: selectArtists(state),
});
const mapDispatchToProps = dispatch => ({
  fetchArtists: () => {
    dispatch(fetchArtists());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(PreFetchData);
