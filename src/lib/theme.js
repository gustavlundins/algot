import palette from './colors';

const breakpoints = ['576px', '768px', '992px', '1200px'];

const space = [0, 8, 16, 32, 48, 64, 96];

const fontSizes = [10, 12, 14, 16, 20, 24, 28, 36, 40, 64, 72];

const fontWeights = [300, 500];

const lineHeights = [1.5, 1.75];

const colors = {
  darkGray: palette.gray[9],
  mediumGray: palette.gray[5],
  lightGray: palette.gray[4],
  lighterGray: palette.gray[1],
  lightestGray: palette.gray[0],
  ...palette
};

const mq = breakpoints.map(bp => `@media (min-width: ${bp})`);

export default {
  breakpoints,
  mq,
  space,
  fontSizes,
  colors,
  lineHeights,
  fontWeights
};
