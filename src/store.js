import {createStore, applyMiddleware, combineReducers} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import artistsReducer from './store/artists'
import startReducer from './store/start'
import newsReducer from './store/news'
import aboutReducer from './store/about'
import thunk from 'redux-thunk'
 
const reducer = combineReducers({
  artists: artistsReducer,
  start: startReducer,
  news: newsReducer,
  about: aboutReducer
})

export default createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(thunk)
  )
)
