const ABOUT_LOADING = 'ABOUT_LOADING';
const ABOUT_LOADED = 'ABOUT_LOADED';

export const selectAboutData = state => {
  return state.about.data;
}

const initState = {
  isLoading: false
}

export default (state = initState, action) => {
  switch (action.type) {
    case ABOUT_LOADING:
      return {...state, isLoading: true}
    case ABOUT_LOADED:
      return {...state, isLoading: false, data : action.data}
    default:
      return state
  }
}
