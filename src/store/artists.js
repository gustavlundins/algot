const initState = {
  artists: [],
  isLoading: false
}

const ARTISTS_LOADING = 'ARTISTS_LOADING';
const ARTISTS_LOADED = 'ARTISTS_LOADED';

export const selectArtists = state => {
  return state.artists.artists;
}

export const selectArtist = (state, props) => {
  if(state.artists.artists){
    const artist = state.artists.artists.findIndex(x => x.urlTitle.toLowerCase() === props);
    if(artist > -1){
      return state.artists.artists[artist];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export default (state = initState, action) => {
  switch (action.type) {
    case ARTISTS_LOADING:
      return {...state, isLoading: true}
    case ARTISTS_LOADED:
      return {...state, isLoading: false, artists: action.artists}
    default:
      return state
  }
}
