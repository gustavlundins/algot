const NEWS_LOADING = 'NEWS_LOADING';
const NEWS_LOADED = 'NEWS_LOADED';

export const selectNews = state => {
  return state.news.news;
}

const initState = {
  isLoading: false,
  news: []
}

export default (state = initState, action) => {
  switch (action.type) {
    case NEWS_LOADING:
      return {...state, isLoading: true}
    case NEWS_LOADED:
      return {...state, isLoading: false, news: action.news}
    default:
      return state
  }
}
