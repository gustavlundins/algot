const START_LOADING = 'START_LOADING';
const START_LOADED = 'START_LOADED';

export const selectStartData = state => {
  return state.start;
}
export const selectBuyLink = state => {
  return state.start.buy_tickets ? state.start.buy_tickets : '/';
}
export const selectStartInfo = state => {
  return {
    info1 : {
      text: state.start.info1_text,
      internal: state.start.info1_internal,
      path: state.start.info1_path
    },
    info2 : {
      text: state.start.info2_text,
      internal: state.start.info2_internal,
      path: state.start.info2_path
    }
  }
}
export const selectSocialInfo = state => {
  if(state.start.instagram){
    return {
      instagram: state.start.instagram,
      facebook: state.start.facebook,
      buy: state.start.buy_tickets
    }
  }
}

const initState = {
  isLoading: false
}

export default (state = initState, action) => {
  switch (action.type) {
    case START_LOADING:
      return {...state, isLoading: true}
    case START_LOADED:
      return {...state, isLoading: false, ...action.data}
    default:
      return state
  }
}
